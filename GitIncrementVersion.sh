#!/usr/bin/env bash

## Usage:
## ./GitIncrementVersion.sh -v 5.6.7-DEV
##
## Arguments:
##    -v: Version string
##    -s: Optional Special tag, the tag at the end of the String eg: DEV

while getopts ":j:s:" opt; do
    case "${opt}" in
        j) OPT_JOB=${OPTARG};;
        s) OPT_SPECIAL=${OPTARG};;
        \? ) echo "Usage: cmd [-j] [-s]"
      ;;
    esac
done

DEFAULT_VERSION="0.0.1-DEV"
HAS_TAGS=$(git tag -l)

if [ -n "$HAS_TAGS" ]; then
    OLD_VER=$(git describe --tags --abbrev=0)
else
    OLD_VER=$DEFAULT_VERSION
fi

RE='[^0-9]*\([0-9]*\)[.]\([0-9]*\)[.]\([0-9]*\)\([0-9A-Za-z-]*\)'
eval MAJOR=`echo $OLD_VER | sed -e "s#$RE#\1#"`
eval MINOR=`echo $OLD_VER | sed -e "s#$RE#\2#"`
eval PATCH=`echo $OLD_VER | sed -e "s#$RE#\3#"`
eval SPECIAL=`echo $OLD_VER | sed -e "s#$RE#\4#"`

if ! [ "${MAJOR}" -eq "${MAJOR}" ] 2>/dev/null; then
	echo $DEFAULT_VERSION
    exit
fi

if ! [ "${MINOR}" -eq "${MINOR}" ] 2>/dev/null; then
	echo $DEFAULT_VERSION
    exit
fi
    
if ! [ "${PATCH}" -eq "${PATCH}" ] 2>/dev/null; then
	echo $DEFAULT_VERSION
    exit
fi

if [ "${OPT_JOB,,}" = "major" ]; then
    let "MAJOR=MAJOR+=1"
fi

if [ "${OPT_JOB,,}" = "minor" ]; then
    let "MINOR=MINOR+=1"
fi

if [ "${OPT_JOB,,}" = "patch" ]; then
    let "PATCH=PATCH+=1"
fi

if [ ! -z "$OPT_SPECIAL" ]; then
    echo "${MAJOR}.${MINOR}.${PATCH}-$OPT_SPECIAL"
elif  [ -z "$SPECIAL" ]; then
    echo "${MAJOR}.${MINOR}.${PATCH}"
else
    echo "${MAJOR}.${MINOR}.${PATCH}-${SPECIAL:1}"
fi
